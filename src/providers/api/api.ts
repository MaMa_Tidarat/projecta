import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoadingController, Loading } from 'ionic-angular';

/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiProvider {
  /**
   * URL API
   */
  private urlAPI: string = 'https://randomuser.me/api/?';
  constructor(public http: HttpClient ,private loader: LoadingController) {
    // console.log('Hello ApiProvider Provider');
  }

  public callAPI(path: string ,method: string ='get' ,request?: any): Promise<any> {
    return new Promise((resolve,reject) =>{
      const loading = this.loader.create({
        content: 'กรุณารอสักครู่...'
      });
      loading.present();
      const url : string = this.urlAPI + path;
      switch(method) {
        case 'get':
            const url : string = this.urlAPI + path +request[0] +'&gender='+request[1].toLowerCase();;
            // console.log(url);
            this.http.get(url).subscribe(res=>{
              loading.dismiss();
                resolve(res['results']);
            });
          break;
        case 'post':
            this.http.post(url,request).subscribe(res=>{
              loading.dismiss();
              resolve(res['results']);
            })
              break;
        default :
          loading.dismiss;
          reject('API Fail');
      }
    });

    
  }

}
