import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { Vibration } from '@ionic-native/vibration';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  private responseApi;
  /**
   * output จาก component
   */
  private listNum: number = 10;
  /**
   * array gender
   */
  private genderChoose: string = 'All';
  public genderList: Array<string> =["All","Male","Female"];

  
  constructor(public navCtrl: NavController, private api: ApiProvider,private vibration: Vibration,private modalCtrl: ModalController) {
    this.callApi(this.listNum, this.genderChoose)
  }
  /**
   * รับ event จาก component
   * @param numberChoose 
   * @param genderChoose 
   */
  public numberOutput(numberChoose: number, genderChoose: string) {
    this.listNum =numberChoose;
    this.callApi(numberChoose, genderChoose)
  }
  /**
   * เรียก API 
   * @param numberChoose  
   * @param genderChoose 
   */
  public callApi(numberChoose: number, genderChoose: string): void {
    let genderNumberList = [numberChoose, genderChoose];
    this.api.callAPI('results=', 'get', genderNumberList).then(res => {
      this.vibration.vibrate(2000);
      this.responseApi = res;
    }, err => {
      alert('can not connect api');
    });
  }
  /**
  * เปลี่ยนไปหน้า profile 
  * @param dataprofile 
  */
  public viewProfile(dataprofile: any): void {
   // this.navCtrl.push('ProfilePage', dataprofile);
   let modal = this.modalCtrl.create('ProfilePage',dataprofile);
    modal.present();
  }
}



