import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  private profile: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
       this.profile = this.navParams.data ; 
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }
  public close():void {
    this.navCtrl.pop(); // เอา page สุดท้ายออก first in last out 
  }

}
