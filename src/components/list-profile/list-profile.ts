
import { NavController } from 'ionic-angular';
import { ApiProvider } from './../../providers/api/api';
import { Component, Input, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { Vibration } from '@ionic-native/vibration';

/**
 * Generated class for the ListProfileComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'list-profile',
  templateUrl: 'list-profile.html'
})
export class ListProfileComponent {

  public numListArr: Array<number> =[10,20,30,50,100];
  public numList: number ;
  /**
   *  จำนวนโปรไฟล์ที่ต้องการโชวร์ ค่าออก 
   */
  @Output() numberChoose: EventEmitter<number> = new EventEmitter;

   constructor(private api:ApiProvider ,private navCtrl: NavController,private vibration: Vibration) {
   this.numberList(10);
  }

  public numberList(numberList: number): void{
    this.numberChoose.emit(numberList);
    this.numList=numberList;
  }



}
