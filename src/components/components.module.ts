import { NgModule } from '@angular/core';
import { ListProfileComponent } from './list-profile/list-profile';
import { IonicModule } from 'ionic-angular';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
@NgModule({
	declarations: [ListProfileComponent],
	imports: [
		IonicModule,
		CommonModule,
		FormsModule
	   ],
	exports: [ListProfileComponent]
})
export class ComponentsModule {}
